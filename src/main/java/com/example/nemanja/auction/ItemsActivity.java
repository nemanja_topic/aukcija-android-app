package com.example.nemanja.auction;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.nemanja.auction.model.Item;
import com.example.nemanja.auction.model.ItemCollection;

import java.util.ArrayList;

public class ItemsActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ListView listView = (ListView)findViewById(R.id.listView);
        CustomAdapter customAdapter = new CustomAdapter(this, ItemCollection.getItemCollection());
        listView.setAdapter(customAdapter);
    }

    public class CustomAdapter extends BaseAdapter{

        Context context;
        ArrayList<Item> itemCollection;

        public CustomAdapter(Context context, ArrayList<Item> itemCollection) {
            this.context = context;
            this.itemCollection = itemCollection;
        }

        @Override
        public int getCount() {
            return itemCollection.size();
        }

        @Override
        public Object getItem(int position) {
            return itemCollection.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.listview_items,parent,false);
            TextView text1 = (TextView)convertView.findViewById(R.id.name);
            TextView text2 = (TextView)convertView.findViewById(R.id.description);
            ImageView image1 = (ImageView)convertView.findViewById(R.id.picture);

            final String name = itemCollection.get(position).getName();
            final String description = itemCollection.get(position).getDescription();
            //final int slika = Resources.getSystem().getIdentifier(itemCollection.get(position).getPicture(), "drawable", "com.Auction");
            final int pic = itemCollection.get(position).getPicture();
            text1.setText(name);
            text2.setText(description);
            image1.setImageResource(pic);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startItemTab1(name, description, pic);
                }
            });
            return convertView;
        }

        private void startItemTab1(String name, String description, int pic){
            Intent intent = new Intent(context, ItemActivity.class);

            intent.putExtra("NAME_KEY", name);
            intent.putExtra("DESCRIPTION_KEY", description);
            intent.putExtra("PIC_KEY", pic);

            context.startActivity(intent);
        }
    }

    @Override
    protected void onStart(){
        super.onStart();
    }

    @Override
    protected void onRestart(){
        super.onRestart();
    }

    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    protected void onStop(){
        super.onStop();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.items, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent settingsActivity = new Intent(this, SettingsActivity.class);
            startActivity(settingsActivity);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.itemsActivity) {

        } else if (id == R.id.auctionsActivity) {
            Intent auctionsActivity = new Intent(this, AuctionsActivity.class);
            startActivity(auctionsActivity);
        } else if (id == R.id.settingsActivity) {
            Intent settingsActivity = new Intent(this, SettingsActivity.class);
            startActivity(settingsActivity);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
