package com.example.nemanja.auction;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.nemanja.auction.model.SplashScreenDuration;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import com.example.nemanja.auction.model.Auction;
import com.example.nemanja.auction.model.Bid;
import com.example.nemanja.auction.model.Item;
import com.example.nemanja.auction.model.User;
import com.j256.ormlite.table.TableUtils;

/**
 * Created by Nemanja on 6/4/17.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private Dao<SplashScreenDuration, Integer> splashScreenDao;
    private Dao<Auction, Integer> auctionDao;
    private Dao<Bid, Integer> bidDao;
    private Dao<Item, Integer> itemDao;
    private Dao<User, Integer> userDao;


    public DatabaseHelper(Context context) {
        super(context, "auction.db", null, 1, R.raw.ormlite_config);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, SplashScreenDuration.class);
            SplashScreenDuration ssd = new SplashScreenDuration(0);
            getSplashScreenDao().create(ssd);
            TableUtils.createTable(connectionSource, User.class);
            TableUtils.createTable(connectionSource, Item.class);
            TableUtils.createTable(connectionSource, Auction.class);
            TableUtils.createTable(connectionSource, Bid.class);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Unable to create datbases", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int oldVer, int newVer) {

        try {
            TableUtils.createTable(connectionSource, SplashScreenDuration.class);
            TableUtils.dropTable(connectionSource, User.class, true);
            TableUtils.dropTable(connectionSource, Item.class, true);
            TableUtils.dropTable(connectionSource, Auction.class, true);
            TableUtils.dropTable(connectionSource, Bid.class, true);
            onCreate(sqLiteDatabase, connectionSource);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Unable to upgrade database from version " + oldVer + " to new "
                    + newVer, e);
        }
    }

/*    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table USERS(id integer primary key autoincrement, name text not null, email text not null, password text not null, picture text, address text, phone text)");
        db.execSQL("create table ITEMS(id integer primary key autoincrement, name text not null, description text not null, picture text not null, sold integer not null)");
        db.execSQL("create table AUCTIONS(id integer primary key autoincrement, startprice real not null, startdate text not null, enddate text, FOREIGN KEY(user_id) REFERENCES USERS(id) , FOREIGN KEY(item_id) REFERENCES ITEMS(id)");
        db.execSQL("create table BIDS(id integer primary key autoincrement, price real not null, date text, FOREIGN KEY(auction_id) REFERENCES AUCTIONS(id), FOREIGN KEY(user_id) REFERENCES USERS(id)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists USERS");
        db.execSQL("drop table if exists ITEMS");
        db.execSQL("drop table if exists AUCTIONS");
        db.execSQL("drop table if exists BIDS");
        onCreate(db);
    }*/

    public Dao<SplashScreenDuration, Integer> getSplashScreenDao() throws SQLException {
        if (splashScreenDao == null) {
            splashScreenDao = getDao(SplashScreenDuration.class);
        }
        return splashScreenDao;
    }

    public Dao<Auction, Integer> getAuctionDao() throws SQLException {
        if (auctionDao == null) {
            auctionDao = getDao(Auction.class);
        }
        return auctionDao;
    }

    public Dao<Bid, Integer> getBidDao() throws SQLException {
        if (bidDao == null) {
            bidDao = getDao(Bid.class);
        }
        return bidDao;
    }

    public Dao<Item, Integer> getItemDao() throws SQLException {
        if (itemDao == null) {
            itemDao = getDao(Item.class);
        }
        return itemDao;
    }

    public Dao<User, Integer> getUserDao() throws SQLException {
        if (userDao == null) {
            userDao = getDao(User.class);
        }
        return userDao;
    }
}