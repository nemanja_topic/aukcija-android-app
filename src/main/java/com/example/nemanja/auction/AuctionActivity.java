package com.example.nemanja.auction;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class AuctionActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auction);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //INIT VIEWS
        TextView tvPrice = (TextView) findViewById(R.id.lblPrice);
        TextView tvStartDate = (TextView) findViewById(R.id.lblStartDate);
        TextView tvEndDate = (TextView) findViewById(R.id.lblEndDate);
        TextView tvUserName = (TextView) findViewById(R.id.lblUserName);
        TextView tvItemName = (TextView) findViewById(R.id.lblItemName);

        Intent intent = this.getIntent();

        String startPrice = intent.getExtras().getString("START_PRICE_KEY");
        String startDate = intent.getExtras().getString("START_DATE_KEY");
        String endDate = intent.getExtras().getString("END_DATE_KEY");
        String userName = intent.getExtras().getString("USER_NAME_KEY");
        String itemName = intent.getExtras().getString("ITEM_NAME_KEY");

        tvPrice.setText(startPrice + "$");
        tvStartDate.setText(startDate);
        tvEndDate.setText(endDate);
        tvUserName.setText(userName);
        tvItemName.setText(itemName);
    }

    public static class AuctionLayout extends Fragment {
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.content_auction, container, false);

            TextView tvPrice = (TextView) rootView.findViewById(R.id.lblPrice);
            TextView tvStartDate = (TextView) rootView.findViewById(R.id.lblStartDate);
            TextView tvEndDate = (TextView) rootView.findViewById(R.id.lblEndDate);
            TextView tvUserName = (TextView) rootView.findViewById(R.id.lblUserName);
            TextView tvItemName = (TextView) rootView.findViewById(R.id.lblItemName);

            String startPrice = getActivity().getIntent().getExtras().getString("START_PRICE_KEY");
            String startDate = getActivity().getIntent().getExtras().getString("START_DATE_KEY");
            String endDate = getActivity().getIntent().getExtras().getString("END_DATE_KEY");
            String userName = getActivity().getIntent().getExtras().getString("USER_NAME_KEY");
            String itemName = getActivity().getIntent().getExtras().getString("ITEM_NAME_KEY");

            tvPrice.setText(startPrice);
            tvStartDate.setText(startDate);
            tvEndDate.setText(endDate);
            tvUserName.setText(userName);
            tvItemName.setText(itemName);

            return rootView;
        }
    }

    @Override
    protected void onStart(){
        super.onStart();
    }

    @Override
    protected void onRestart(){
        super.onRestart();
    }

    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    protected void onStop(){
        super.onStop();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.auction, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent settingsActivity = new Intent(this, SettingsActivity.class);
            startActivity(settingsActivity);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.itemsActivity) {
            Intent itemsActivity = new Intent(this, ItemsActivity.class);
            startActivity(itemsActivity);
        } else if (id == R.id.auctionsActivity) {
            Intent auctionsActivity = new Intent(this, AuctionsActivity.class);
            startActivity(auctionsActivity);
        } else if (id == R.id.settingsActivity) {
            Intent settingsActivity = new Intent(this, SettingsActivity.class);
            startActivity(settingsActivity);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}