package com.example.nemanja.auction.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by Nemanja on 6/20/17.
 */
@DatabaseTable(tableName = "SPLASHSCREEN")
public class SplashScreenDuration implements Serializable {
    @DatabaseField(generatedId = true, columnName = "id", dataType = DataType.INTEGER)
    private int id;
    @DatabaseField(columnName = "duration", dataType = DataType.INTEGER)
    private int duration;

    public SplashScreenDuration(int id, int duration){
        this.id = id;
        this.duration = duration;
    }

    public SplashScreenDuration(int duration){
        this.duration = duration;
    }

    public SplashScreenDuration(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "SplashScreenDuration{" +
                "id=" + id +
                ", duration=" + duration +
                '}';
    }
}