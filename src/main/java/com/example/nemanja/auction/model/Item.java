package com.example.nemanja.auction.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Nemanja on 5/22/17.
 */
@DatabaseTable(tableName = "ITEMS")
public class Item implements Serializable {
    @DatabaseField(generatedId = true, columnName = "id", dataType = DataType.INTEGER)
    private long id;
    @DatabaseField(columnName = "name", dataType = DataType.STRING, canBeNull = false)
    private String name;
    @DatabaseField(columnName = "description", dataType = DataType.STRING, canBeNull = false)
    private String description;
    @DatabaseField(columnName = "picture", dataType = DataType.STRING, canBeNull = false)
    private int picture;
    @DatabaseField(columnName = "sold", dataType = DataType.INTEGER, canBeNull = false)
    private boolean sold;
    private List<Auction> auctions;

    public Item(long id, String name, String description, int picture, boolean sold, List<Auction> auctions) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.picture = picture;
        this.sold = sold;
        this.auctions = auctions;
    }

    public Item(long id, String name, String description, int picture, boolean sold) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.picture = picture;
        this.sold = sold;
    }

    public Item(){

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPicture() {
        return picture;
    }

    public void setPicture(int picture) {
        this.picture = picture;
    }

    public boolean isSold() {
        return sold;
    }

    public void setSold(boolean sold) {
        this.sold = sold;
    }

    public List<Auction> getAuctions() {
        return auctions;
    }

    public void setAuctions(List<Auction> auctions) {
        this.auctions = auctions;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", picture='" + picture + '\'' +
                ", sold=" + sold +
                ", auctions=" + auctions +
                '}';
    }
}
