package com.example.nemanja.auction.model;

import com.example.nemanja.auction.R;

import java.util.ArrayList;

/**
 * Created by Nemanja on 5/28/17.
 */

public class ItemCollection {

    public static Item item1 = new Item(1, "Mona Lisa", "Original painting", R.drawable.monalisa, false, AuctionCollection.getAuctionCollection());
    public static Item item2 = new Item(2, "Just a baby's prayer at twilight", "Book written by Henry Burr", R.drawable.book, false, AuctionCollection.getAuctionCollection());
    public static Item item3 = new Item(3, "Old Camera", "A really old camera that still works", R.drawable.camera, false, AuctionCollection.getAuctionCollection());

    public static ArrayList<Item> getItemCollection(){
        ArrayList<Item> itemsCollection = new ArrayList<Item>();

        itemsCollection.add(item1);
        itemsCollection.add(item2);
        itemsCollection.add(item3);

        return itemsCollection;
    }
}
