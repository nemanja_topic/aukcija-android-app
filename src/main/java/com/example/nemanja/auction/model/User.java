package com.example.nemanja.auction.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Nemanja on 5/22/17.
 */
@DatabaseTable(tableName = "USERS")
public class User implements Serializable {
    @DatabaseField(generatedId = true, columnName = "id", dataType = DataType.INTEGER)
    private long id;
    @DatabaseField(columnName = "name", dataType = DataType.STRING, canBeNull = false)
    private String name;
    @DatabaseField(columnName = "email", dataType = DataType.STRING, canBeNull = false)
    private String email;
    @DatabaseField(columnName = "password", dataType = DataType.STRING, canBeNull = false)
    private String password;
    @DatabaseField(columnName = "picture", dataType = DataType.STRING, canBeNull = true)
    private String picture;
    @DatabaseField(columnName = "address", dataType = DataType.STRING, canBeNull = true)
    private String address;
    @DatabaseField(columnName = "phone", dataType = DataType.STRING, canBeNull = true)
    private String phone;
    private List<Auction> auctions;
    private List<Bid> bids;



    public User(long id, String name, String email, String password, String picture, String address, String phone, List<Auction> auctions, List<Bid> bids) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.picture = picture;
        this.address = address;
        this.phone = phone;
        this.auctions = auctions;
        this.bids = bids;
    }

    public User(long id, String name, String email, String password, String picture, String address, String phone) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.picture = picture;
        this.address = address;
        this.phone = phone;
    }

    public User(){

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<Auction> getAuctions() {
        return auctions;
    }

    public void setAuctions(List<Auction> auctions) {
        this.auctions = auctions;
    }

    public List<Bid> getBids() {
        return bids;
    }

    public void setBids(List<Bid> bids) {
        this.bids = bids;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", picture='" + picture + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", auctions=" + auctions +
                ", bids=" + bids +
                '}';
    }
}