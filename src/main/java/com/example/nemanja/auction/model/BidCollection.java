package com.example.nemanja.auction.model;

import java.util.ArrayList;
import java.util.Date;

import static com.example.nemanja.auction.model.AuctionCollection.auction1;
import static com.example.nemanja.auction.model.AuctionCollection.auction2;
import static com.example.nemanja.auction.model.AuctionCollection.auction3;
import static com.example.nemanja.auction.model.AuctionCollection.user;

/**
 * Created by Nemanja on 5/28/17.
 */

public class BidCollection {

    public static ArrayList<Bid> getBidCollection1(){
        ArrayList<Bid> bidCollection = new ArrayList<Bid>();
        Bid bid1 = new Bid(1, 5000.50, new Date(), auction1, user);
        Bid bid2 = new Bid(2, 1500.35, new Date(), auction1, user);
        Bid bid3 = new Bid(3, 7500.43, new Date(), auction1, user);

        return bidCollection;
    }

    public static ArrayList<Bid> getBidCollection2(){
        ArrayList<Bid> bidCollection = new ArrayList<Bid>();
        Bid bid1 = new Bid(1, 5305.53, new Date(), auction2, user);
        Bid bid2 = new Bid(2, 9568.57, new Date(), auction2, user);
        Bid bid3 = new Bid(3, 2345.56, new Date(), auction2, user);

        return bidCollection;
    }

    public static ArrayList<Bid> getBidCollection3(){
        ArrayList<Bid> bidCollection = new ArrayList<Bid>();
        Bid bid1 = new Bid(1, 2545.56, new Date(), auction3, user);
        Bid bid2 = new Bid(2, 3456.53, new Date(), auction3, user);
        Bid bid3 = new Bid(3, 5340.50, new Date(), auction3, user);

        return bidCollection;
    }
}
