package com.example.nemanja.auction.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Nemanja on 5/22/17.
 */

@DatabaseTable(tableName = "BIDS")
public class Bid implements Serializable {
    @DatabaseField(generatedId = true, columnName = "id", dataType = DataType.INTEGER)
    private long id;
    @DatabaseField(columnName = "price", dataType = DataType.DOUBLE)
    private double price;
    @DatabaseField(columnName = "date", dataType = DataType.DATE)
    private Date dateTime;
    @DatabaseField(canBeNull = false, foreign = true, foreignAutoRefresh = true)
    private Auction auction;
    @DatabaseField(canBeNull = false, foreign = true, foreignAutoRefresh = true)
    private User user;

    public Bid(long id, double price, Date dateTime, Auction auction, User user) {
        this.id = id;
        this.price = price;
        this.dateTime = dateTime;
        this.auction = auction;
        this.user = user;
    }

    public Bid(){

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Bid{" +
                "id=" + id +
                ", price=" + price +
                ", dateTime=" + dateTime +
                ", auction=" + auction +
                ", user=" + user +
                '}';
    }
}
