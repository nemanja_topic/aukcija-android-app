package com.example.nemanja.auction.model;

import com.example.nemanja.auction.R;

import java.util.ArrayList;
import java.util.Date;

import static com.example.nemanja.auction.model.ItemCollection.item1;
import static com.example.nemanja.auction.model.ItemCollection.item2;
import static com.example.nemanja.auction.model.ItemCollection.item3;

/**
 * Created by Nemanja on 5/28/17.
 */

public class AuctionCollection {

    public static User user = new User(1, "Nemanja Topić", "nemanja_topic@hotmail.rs", "password", "R.mipmap.ic_launcher", "Pavla Vujisica 11", "+387640208815", new ArrayList<Auction>(), new ArrayList<Bid>());

    public static Item item1 = new Item(1, "Mona Lisa", "Original painting", R.drawable.monalisa, false, AuctionCollection.getAuctionCollection());
    public static Item item2 = new Item(2, "Just a baby's prayer at twilight", "Book written by Henry Burr", R.drawable.book, false, AuctionCollection.getAuctionCollection());
    public static Item item3 = new Item(3, "Old Camera", "A really old camera that still works", R.drawable.camera, false, AuctionCollection.getAuctionCollection());

    public static Auction auction1 = new Auction(1, 10000, new Date(), new Date(), user, item1, BidCollection.getBidCollection1());
    public static Auction auction2 = new Auction(1, 5000, new Date(), new Date(), user, item2, BidCollection.getBidCollection2());
    public static Auction auction3 = new Auction(1, 2500, new Date(), new Date(), user, item3, BidCollection.getBidCollection3());

    public static ArrayList<Auction> getAuctionCollection(){
        ArrayList<Auction> auctionCollection = new ArrayList<Auction>();

        auctionCollection.add(auction1);
        auctionCollection.add(auction2);
        auctionCollection.add(auction3);

        return auctionCollection;
    }
}
