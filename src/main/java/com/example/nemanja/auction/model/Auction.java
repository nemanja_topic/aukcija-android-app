package com.example.nemanja.auction.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Nemanja on 5/22/17.
 */

@DatabaseTable(tableName = "AUCTIONS")
public class Auction implements Serializable {
    @DatabaseField(generatedId = true, columnName = "id", dataType = DataType.INTEGER)
    private long id;
    @DatabaseField(columnName = "startprice", dataType = DataType.DOUBLE)
    private double startPrice;
    @DatabaseField(columnName = "startdate", dataType = DataType.DATE)
    private Date startDate;
    @DatabaseField(columnName = "enddate", dataType = DataType.DATE)
    private Date endDate;
    @DatabaseField(canBeNull = false, foreign = true, foreignAutoRefresh = true)
    private User user;
    @DatabaseField(canBeNull = false, foreign = true, foreignAutoRefresh = true)
    private Item item;
    private List<Bid> bids;

    public Auction(long id, double startPrice, Date startDate, Date endDate, User user, Item item, List<Bid> bids) {
        this.id = id;
        this.startPrice = startPrice;
        this.startDate = startDate;
        this.endDate = endDate;
        this.user = user;
        this.item = item;
        this.bids = bids;
    }

    public Auction(long id, double startPrice, Date startDate, Date endDate, User user, Item item) {
        this.id = id;
        this.startPrice = startPrice;
        this.startDate = startDate;
        this.endDate = endDate;
        this.user = user;
        this.item = item;
    }

    public Auction(){

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(double startPrice) {
        this.startPrice = startPrice;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public List<Bid> getBids() {
        return bids;
    }

    public void setBids(List<Bid> bids) {
        this.bids = bids;
    }

    @Override
    public String toString() {
        return "Auction{" +
                "id=" + id +
                ", startPrice=" + startPrice +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", user=" + user +
                ", item=" + item +
                ", bids=" + bids +
                '}';
    }
}
