package com.example.nemanja.auction;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.nemanja.auction.model.Auction;
import com.example.nemanja.auction.model.AuctionCollection;
import com.example.nemanja.auction.model.Item;
import com.example.nemanja.auction.model.User;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AuctionsActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    String[] PRICE = {"5000€", "2500€", "500€"};
    String[] ITEMS = {"Mona Lisa", "Item 2", "Item 3"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auctions);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ListView listView = (ListView)findViewById(R.id.listView);
        CustomAdapter customAdapter = new CustomAdapter(this, AuctionCollection.getAuctionCollection());
        listView.setAdapter(customAdapter);
    }

    class CustomAdapter extends BaseAdapter{

        Context context;
        ArrayList<Auction> auctionCollection;

        public CustomAdapter(Context context, ArrayList<Auction> auctionCollection) {
            this.context = context;
            this.auctionCollection = auctionCollection;
        }

        @Override
        public int getCount() {
            return auctionCollection.size();
        }

        @Override
        public Object getItem(int position) {
            return auctionCollection.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(android.R.layout.simple_list_item_2, null);
            TextView text1 = (TextView)convertView.findViewById(android.R.id.text1);
            TextView text2 = (TextView)convertView.findViewById(android.R.id.text2);

            final double startPrice = auctionCollection.get(position).getStartPrice();
            final Date startDate = auctionCollection.get(position).getStartDate();
            final Date endDate = auctionCollection.get(position).getEndDate();
            final User user = auctionCollection.get(position).getUser();
            final Item item = auctionCollection.get(position).getItem();

            final String startPriceString = Double.toString(startPrice);
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            final String startDateString = df.format(startDate);
            final String endDateString = df.format(endDate);
            final String userNameString = user.getName();
            final String itemNameString = item.getName();

            text1.setText(startPriceString + "$");
            text2.setText(item.getName());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startAuctionActivity(startPriceString, startDateString, endDateString, userNameString, itemNameString);
                }
            });
            return convertView;
        }

        private void startAuctionActivity(String startPrice, String startDate, String endDate, String user, String item){
            Intent intent = new Intent(context, AuctionActivity.class);

            intent.putExtra("START_PRICE_KEY", startPrice);
            intent.putExtra("START_DATE_KEY", startDate);
            intent.putExtra("END_DATE_KEY", endDate);
            intent.putExtra("USER_NAME_KEY", user);
            intent.putExtra("ITEM_NAME_KEY", item);

            context.startActivity(intent);
        }
    }

    @Override
    protected void onStart(){
        super.onStart();
    }

    @Override
    protected void onRestart(){
        super.onRestart();
    }

    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    protected void onStop(){
        super.onStop();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.auctions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent settingsActivity = new Intent(this, SettingsActivity.class);
            startActivity(settingsActivity);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.itemsActivity) {
            Intent itemsActivity = new Intent(this, ItemsActivity.class);
            startActivity(itemsActivity);
        } else if (id == R.id.auctionsActivity) {

        } else if (id == R.id.settingsActivity) {
            Intent settingsActivity = new Intent(this, SettingsActivity.class);
            startActivity(settingsActivity);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
